﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuButton : MonoBehaviour 
{
	public Sprite _nomalSprite;
	public Sprite _selectionSprite;
	public Image _image;
	public Button _button;
	public RectTransform _rect;

	void Start () 
	{
	
	}

	void Update () 
	{
	
	}

	public void ActivateButton()
	{
		_button.interactable = true;
	}

	public void DeactivateButton()
	{
		_button.interactable = false;
	}

	public void Select()
	{
		_image.sprite = _selectionSprite;
	}

	public void CancelSelection()
	{
		_image.sprite = _nomalSprite;
	}
}
