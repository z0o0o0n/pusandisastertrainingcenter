﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Menu : MonoBehaviour 
{	
	public delegate void MenuEvent (int id);
	public event MenuEvent Selected;

	public List<MenuButton> menuButtonList;
	public RectTransform selectionMarkRect;
	public int currentId = 0;

	private bool _isActivatedButtons = false;

	void Start () 
	{
	}

	void Update () 
	{
	
	}

	public void ActivateButtons()
	{
		for(int i = 0; i < menuButtonList.Count; i++)
		{
			menuButtonList[i].ActivateButton();
		}
	}

	public void DeactivateButtons()
	{
		for(int i = 0; i < menuButtonList.Count; i++)
		{
			menuButtonList[i].DeactivateButton();
		}
	}

	public void Select(int id)
	{
		OnMenuClicked (id);
	}

	public void OnMenuClicked(int id)
	{
		//Debug.Log ("id: " + id);
		currentId = id;
		for(int i = 0; i < menuButtonList.Count; i++)
		{
			if(i == id)
			{
				menuButtonList[i].Select();
				selectionMarkRect.localPosition = new Vector2(120f * i, 40);

				if(Selected != null) Selected(id);
			}
			else
			{
				menuButtonList[i].CancelSelection();
			}
		}
	}
}
