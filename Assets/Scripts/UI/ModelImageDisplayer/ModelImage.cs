﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class ModelImage : MonoBehaviour 
{
	public delegate void ModelImageEvent(int id);
	public event ModelImageEvent PointerClick;

	public RectTransform rect;
	public List<LabelPointer> labelPointerList;

	private float _labelPointerDisplayValue = 0.0f;
	private int _labelPointerCount = 0;

	void Awake ()
	{
		for (int i = 0; i < labelPointerList.Count; i++)
		{
			labelPointerList[i].PointerClick += OnPointerClick;
		}
	}

	void Start () 
	{
	}

	void Update () 
	{
	
	}

	public void ActivateButtons()
	{
		for (int i = 0; i < labelPointerList.Count; i++)
		{
			labelPointerList[i].ActivatePointer();
		}
	}

	public void DeactivateButtons()
	{
		for (int i = 0; i < labelPointerList.Count; i++)
		{
			labelPointerList[i].DeactivatePointer();
		}
	}

	private void OnPointerClick(int id)
	{
		//model.OpenDetailPage(id);
		if(PointerClick != null) PointerClick(id);
	}

	// 라벨포인터 보이기
	public void ShowLabelPointers()
	{
		_labelPointerCount = 0;
		for (int i = 0; i < labelPointerList.Count; i++)
		{
			labelPointerList[i].DeactivatePointer();
			DOTween.To(() => _labelPointerDisplayValue, x => _labelPointerDisplayValue = x, 1.0f, 0.3f).SetId("LP_interval_" + i + "_" + GetInstanceID()).SetDelay(i * 0.05f).OnComplete(OnLabelPointerShowed);
		}
	}

	private void OnLabelPointerShowed()
	{
		// 각 라벨포인터가 보여졌을 때
		labelPointerList[_labelPointerCount].Show();
		_labelPointerCount++;
		if (_labelPointerCount >= labelPointerList.Count) OnAllLabelPointersShowed();
	}
	
	private void OnAllLabelPointersShowed()
	{
		// 모든 라벨포인터가 보여졌을 때
		for (int i = 0; i < labelPointerList.Count; i++)
		{
			labelPointerList[i].ActivatePointer();
		}
	}
	
	// 모든 라벨포인터를 초기화(숨김상태)
	public void ResetLabelPointers()
	{
		for (int i = 0; i < labelPointerList.Count; i++)
		{
			DOTween.Kill("LP_interval_" + i + "_" + GetInstanceID());
			labelPointerList[i].Reset();
		}
	}
	
	void OnDestory()
	{
		for (int i = 0; i < labelPointerList.Count; i++)
		{
			labelPointerList[i].PointerClick -= OnPointerClick;
		}
	}
}
