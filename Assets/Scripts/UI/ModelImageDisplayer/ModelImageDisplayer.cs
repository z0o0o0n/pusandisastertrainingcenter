﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class ModelImageDisplayer : MonoBehaviour 
{
	public delegate void ModelImageDisplayerEvent();
	public event ModelImageDisplayerEvent Changed;
	public event ModelImage.ModelImageEvent PointerClick;

	public List<ModelImage> modelImageList;

	private int _currentIndex = 0;
	private int _prevIndex = 0;

	void Awake ()
	{
		for(int i = 0; i < modelImageList.Count; i++)
		{
			modelImageList[i].PointerClick += OnPointerClick;
		}
	}

	void Start () 
	{
		modelImageList[_currentIndex].ShowLabelPointers ();
	}

	void Update () 
	{
		if(Input.GetKeyUp(KeyCode.LeftArrow))
		{
			Change(Random.Range(0, 5));
		}
	}

	void OnDestroy()
	{
		for(int i = 0; i < modelImageList.Count; i++)
		{
			modelImageList[i].PointerClick -= OnPointerClick;
		}
	}

	public void ActivateButtons()
	{
		for(int i = 0; i < modelImageList.Count; i++)
		{
			modelImageList[i].ActivateButtons();
		}
	}

	public void DeactivateButtons()
	{
		for(int i = 0; i < modelImageList.Count; i++)
		{
			modelImageList[i].DeactivateButtons();
		}
	}

	private void OnPointerClick(int id)
	{
		if(PointerClick != null) PointerClick(id);
	}

	public void Change(int id)
	{
		Debug.Log ("-----id: " + id);

		int arrow = 0;
		if(_currentIndex == id) 
		{
			arrow = 0;
			if(Changed != null) Changed();
			return;
		}
		else if(_currentIndex < id) arrow = -1;
		else if(_currentIndex > id) arrow = 1;

		modelImageList[_currentIndex].rect.DOLocalMoveX (2048f * arrow, 0.5f).SetEase (Ease.InOutQuart);

		modelImageList[id].gameObject.SetActive (true);
		Vector2 nextModelImagePos = modelImageList[id].rect.localPosition;
		nextModelImagePos.x = -2048f * arrow;
		modelImageList[id].rect.localPosition = nextModelImagePos;
		modelImageList[id].rect.DOLocalMoveX (0.0f, 0.5f).SetEase (Ease.InOutQuart).OnComplete(OnChanged);

		_prevIndex = _currentIndex;
		_currentIndex = id;
	}

	private void OnChanged()
	{
		Debug.Log ("===== changed");
		modelImageList[_prevIndex].ResetLabelPointers();
		//modelImageList[_prevIndex].gameObject.SetActive(false);

		modelImageList[_currentIndex].ShowLabelPointers();

		if(Changed != null) Changed();
	}

	public void ShowLabelPointers()
	{
		modelImageList[_currentIndex].ShowLabelPointers();
	}
	
	public void ResetLabelPointers()
	{
		modelImageList[_currentIndex].ResetLabelPointers();
	}
}
