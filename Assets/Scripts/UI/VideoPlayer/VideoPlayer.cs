﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using RenderHeads.Media.AVProVideo;

public class VideoPlayer : MonoBehaviour 
{
    //public MediaPlayerCtrl videoController;
	public MediaPlayer mediaPlayer;
    public DisplayUGUI display;
    public Slider slider;
    public string videoId;

    private bool _isPointerDown = false;
	private bool _isFinished = false;
    private bool _isActivatedButtons = false;

    void Awake ()
    {
    }

	void Start () 
	{
	}
	
	void Update () 
	{
		if(display.CurrentMediaPlayer != null)
		{
			if(!_isPointerDown)
	        {
				if(mediaPlayer.Control.GetCurrentTimeMs() == mediaPlayer.Info.GetDurationMs() && mediaPlayer.Info.GetDurationMs() > 0) 
				{
					if(!_isFinished)
					{
						_isFinished = true;
						Debug.Log ("End");
						ActivationCheckManager.Run();
					}
				}
				else
				{
					if(_isFinished)
					{
						_isFinished = false;
						Debug.Log ("Plaing");
						ActivationCheckManager.Stop();
					}
				}
            	
				slider.value = ConvertTimeToValue(mediaPlayer.Control.GetCurrentTimeMs());
	        }
		}
	}

    public void ActivateButtons()
    {
        _isActivatedButtons = true;
        slider.interactable = true;
    }

    public void DeactivateButton()
    {
        _isActivatedButtons = false;
        slider.interactable = false;
    }

    private void OnVideoEnd()
    {
        //videoController.SeekTo(0);
    }

    public void OnPointerDown()
    {
        if (_isActivatedButtons)
        {
            _isPointerDown = true;
            mediaPlayer.Control.Pause();
        }
    }

    public void OnPointerUp()
    {
        if (_isActivatedButtons)
        {
            float time = ConvertValueToTime(slider.value);
            _isPointerDown = false;
            mediaPlayer.Control.Seek((int)time);
            mediaPlayer.Control.Play();
        }
    }

    public void Play()
    {
		display.CurrentMediaPlayer = mediaPlayer;
		mediaPlayer.m_VideoPath = string.Empty;
		mediaPlayer.m_VideoPath = "Video_" + videoId + ".mp4";
		mediaPlayer.OpenVideoFromFile();

		ActivationCheckManager.Stop ();
        //Debug.Log("Play Video - videoId: " + videoId);
        //videoController.Load("Video_0.mp4");
        //videoController.m_TargetMaterial[0] = videoRawImage.gameObject;
        //videoController.Play();
        //Debug.Log("height: " + videoRawImage.rectTransform.sizeDelta.y + " / y: " + videoRawImage.uvRect.y + " , h: " + videoRawImage.uvRect.height);
    }

    public void Stop()
    {
		display.CurrentMediaPlayer = null;
		mediaPlayer.Control.Stop ();
		mediaPlayer.Control.Seek (0);
        //Debug.Log("Stop - videoId: " + videoId);
        //videoRawImage.texture = defaultTexture;
        //videoController.Stop();
        ////videoController.UnLoad();
    }

    private float ConvertValueToTime(float sliderValue)
    {
        float time = 0.0f;
		time = mediaPlayer.Info.GetDurationMs() * sliderValue;
        return time;
    }

    private float ConvertTimeToValue(float time)
    {
        float value = 0.0f;
		if (mediaPlayer.Info.GetDurationMs() <= 0) return 0;
		value = time / mediaPlayer.Info.GetDurationMs();
        return value;
    }

    void OnDestroy()
    {
        //videoController.OnEnd -= OnVideoEnd;
    }
}
