﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LabelPointer : MonoBehaviour 
{
    public delegate void LabelPointerEvent(int id);
    public event LabelPointerEvent PointerClick;
    public Pointer pointer;
    public LabelLineResizer lineResizer;
    public NameBoxManager nameBoxManager;
	public int videoId;

    private bool _isActivatedPointer = true;

    void Awake ()
    {
        pointer.Hide(0.0f);
    }

	void Start () 
	{
	}

    public void ActivatePointer()
    {
        _isActivatedPointer = true;
        nameBoxManager.ActivatePointer();
    }

    public void DeactivatePointer()
    {
        _isActivatedPointer = false;
        nameBoxManager.DeactivatePointer();
    }

    public void Show()
    {
        pointer.Show(1.0f);
        lineResizer.Show(0.5f);
        nameBoxManager.Show(1.0f);
    }

    public void Reset()
    {
        pointer.Hide(0.0f);
        lineResizer.Hide(0.0f);
        nameBoxManager.Hide(0.0f);
    }
	
	void Update () 
	{
	
	}

    public void OnPointerClick(int id)
    {
        if (_isActivatedPointer)
        {
			if (PointerClick != null) PointerClick(videoId);
        }
    }
}
