﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class NameBoxManager : MonoBehaviour 
{
    public Image bgImage;
    public Text text;

    private float MARGIN = 50;
    private bool _isActivatedPointer = true;

    void Awake ()
    {
        Hide(0.0f);
    }

	void Start () 
	{

	}
	
	void Update () 
	{
	
	}

    public void OnPointerDown()
    {
        if(_isActivatedPointer)
        {
            Vector2 size = bgImage.rectTransform.sizeDelta;
            size.x += 20;
            size.y += 20;
            bgImage.rectTransform.sizeDelta = size;
        }
    }

    public void OnPointerUp()
    {
        if(_isActivatedPointer)
        {
            Vector2 size = bgImage.rectTransform.sizeDelta;
            size.x -= 20;
            size.y -= 20;
            bgImage.rectTransform.sizeDelta = size;
        }
    }

    public void ActivatePointer()
    {
        _isActivatedPointer = true;
    }

    public void DeactivatePointer()
    {
        _isActivatedPointer = false;
    }

    public void Show(float time, float delay = 0.0f)
    {
		DOTween.Kill("NBM_bgImageColor_" + GetInstanceID());
		DOTween.Kill("NBM_bgImageSize_" + GetInstanceID());
		DOTween.Kill("NBM_textColor_" + GetInstanceID());
		DOTween.Kill("NBM_bgImageScale_" + GetInstanceID());
        if (time == 0.0f)
        {
            bgImage.color = new Color(1, 1, 1, 1);
            bgImage.rectTransform.sizeDelta = new Vector2(140, 100);
        }
        else
        {
            DOTween.To(() => bgImage.color, x => bgImage.color = x, new Color(1, 1, 1, 1), 0.1f).SetId("NBM_bgImageColor_" + GetInstanceID()).SetDelay(delay);
			DOTween.To(() => bgImage.rectTransform.sizeDelta, x => bgImage.rectTransform.sizeDelta = x, new Vector2(text.preferredWidth + (MARGIN*2), 100), 0.5f).SetId("NBM_bgImageSize_" + GetInstanceID()).SetDelay(delay + 0.2f).SetEase(Ease.OutQuart);
			DOTween.To(() => text.color, x => text.color = x, new Color(1, 1, 1, 1), 0.07f).SetId("NBM_textColor_" + GetInstanceID()).SetDelay(delay + 0.5f).SetLoops(5, LoopType.Yoyo);
        }
    }

    public void Hide(float time)
    {
		DOTween.Kill("NBM_bgImageColor_" + GetInstanceID());
		DOTween.Kill("NBM_bgImageSize_" + GetInstanceID());
		DOTween.Kill("NBM_textColor_" + GetInstanceID());
		DOTween.Kill("NBM_bgImageScale_" + GetInstanceID());
        if(time == 0.0f)
        {
            bgImage.color = new Color(1, 1, 1, 0);
            bgImage.rectTransform.sizeDelta = new Vector2(40, 40);
            text.color = new Color(1, 1, 1, 0);
        }
        else
        {
            bgImage.rectTransform.DOScale(0, time).SetId("NBM_bgImageScale_" + GetInstanceID());
        }
    }
}
