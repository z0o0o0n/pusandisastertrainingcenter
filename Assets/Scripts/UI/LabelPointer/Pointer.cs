﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class Pointer : MonoBehaviour 
{
    public Image pointerImage;

    void Awake ()
    {
        Hide(0.0f);
    }

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    public void Show (float time)
    {
		DOTween.Kill("PointerScaleTween_" + GetInstanceID());
        if (time == 0)
        {
            pointerImage.rectTransform.localScale = Vector3.one;
        }
        else
        {
            pointerImage.rectTransform.DOScale(1, time).SetId("PointerScaleTween_" + GetInstanceID()).SetEase(Ease.OutElastic);
        }
    }

    public void Hide (float time)
    {
		DOTween.Kill("PointerScaleTween_" + GetInstanceID());
        if (time == 0)
        {
            pointerImage.rectTransform.localScale = Vector3.zero;
        }
        else
        {
			pointerImage.rectTransform.DOScale(0, time).SetId("PointerScaleTween_" + GetInstanceID());
        }
    }
}
