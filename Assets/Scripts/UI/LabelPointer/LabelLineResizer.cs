﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class LabelLineResizer : MonoBehaviour 
{
    public float targetWidth;
    public GameObject lineContainer;
    public Image lineImage;
    public NameBoxManager nameBoxManager;

    void Awake ()
    {
        // 라인 사이즈 초기화
        lineImage.rectTransform.sizeDelta = new Vector2(0, 9);
    }

	void Start () 
	{
        // 라인 회전
        Vector3 targetPos = nameBoxManager.bgImage.rectTransform.position;
        lineContainer.transform.LookAt(targetPos);
	}
	
	void Update () 
	{
        
	}

    public void Show(float time)
    {
		DOTween.Kill("LabelLineTween_" + GetInstanceID());
        if(time == 0.0f)
        {
            lineImage.rectTransform.sizeDelta = new Vector2(targetWidth, 9);
        }
        else
        {
            DOTween.To(() => lineImage.rectTransform.sizeDelta, x => lineImage.rectTransform.sizeDelta = x, new Vector2(targetWidth, 9), time).SetId("LabelLineTween_" + GetInstanceID()).SetEase(Ease.OutQuart);
        }
    }

    public void Hide(float time)
    {
		DOTween.Kill("LabelLineTween_" + GetInstanceID());
        if (time == 0.0f)
        {
            lineImage.rectTransform.sizeDelta = new Vector2(1, 9);
        }
        else
        {
			DOTween.To(() => lineImage.rectTransform.sizeDelta, x => lineImage.rectTransform.sizeDelta = x, new Vector2(1, 9), time).SetId("LabelLineTween_" + GetInstanceID()).SetEase(Ease.InQuart);
        }
    }
}
