﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class ActivationCheckManager : MonoBehaviour 
{
	public delegate void ActivationCheckEvent();
	public static event ActivationCheckEvent Deactivated;
	public static int checkTime = 3;

	private static int _timeTweenValue = 1;

	void Start () 
	{
		
	}

	void Update () 
	{
	
	}

	// Start Check
	public static void Run()
	{
		Debug.Log ("--- check start");
		_timeTweenValue = 0;
		DOTween.Kill ("CheckTimeTween");
		DOTween.To (() => _timeTweenValue, x => _timeTweenValue = x, 1, checkTime).SetId("CheckTimeTween").SetEase(Ease.Linear).OnComplete(OnDeactivated);
	}

	private static void OnDeactivated()
	{
		Debug.Log ("deactivated");
		if(Deactivated != null) Deactivated();
	}

	// Pause Check
	public static void Stop()
	{
		Debug.Log ("!!! check stop");
		DOTween.Kill ("CheckTimeTween");
	}

	// Update Activation
	public static void UpdateActivation()
	{
		// Restart();
		Run ();
	}
}
