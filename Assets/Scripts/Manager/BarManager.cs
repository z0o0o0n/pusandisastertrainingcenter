﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;

public class BarManager : MonoBehaviour 
{
    public Image topBar;
    public Image bottomBar;

    void Awake ()
    {
        Open(0.0f);
    }

	void Start () 
	{

	}
	
	void Update () 
	{
	
	}

    public void Open (float time)
    {
        topBar.rectTransform.DOLocalMoveY(720f, time).SetEase(Ease.InOutQuart);
        bottomBar.rectTransform.DOLocalMoveY(-720f, time).SetEase(Ease.InOutQuart);
    }

    public void Close (float time)
    {
        topBar.rectTransform.DOLocalMoveY(50f, time).SetEase(Ease.InOutQuart);
        bottomBar.rectTransform.DOLocalMoveY(-50f, time).SetEase(Ease.InOutQuart);
    }
}
