﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class ModelReturnManager : MonoBehaviour 
{
	public delegate void ModelReturnManagerEvent ();
	public event ModelReturnManagerEvent Timeout;

	private float returnTime = 60.0f;
	private float tweenValue = 0.0f;
	void Start () 
	{
	
	}

	void Update ()
	{
	
	}

	public void StartCheck()
	{
		Debug.Log ("Start Check");
		DOTween.Kill ("ReturnCheckTween");
		DOTween.To(()=> tweenValue, x=> tweenValue =x, 0.0f, returnTime).SetId("ReturnCheckTween").OnComplete(OnTimeout);
	}

	private void OnTimeout()
	{
		if(Timeout != null) Timeout();
	}

	public void StopCheck()
	{
		Debug.Log ("Stop Check");
		DOTween.Kill ("ReturnCheckTween");
	}
}
