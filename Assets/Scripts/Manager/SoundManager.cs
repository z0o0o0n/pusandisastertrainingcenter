﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour 
{
	public AudioSource audioSource;
	public AudioClip buttonSound;

	void Start () 
	{
	
	}

	void Update () 
	{
	
	}

	public void PlayButtonSound()
	{
		audioSource.clip = buttonSound;
		audioSource.Play ();
	}
}
