﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using DG.Tweening;
using RenderHeads.Media.AVProVideo;

public class VideoPlayerManager : MonoBehaviour 
{
    public delegate void VideoPlayerManagerEvent(int videoId);
    public event VideoPlayerManagerEvent VideoChanged;
    public event VideoPlayerManagerEvent VideoMoved;

    public RectTransform rectTransform;
    public List<VideoPlayer> videoPlayerList;

    private int _currentVideoId = 0;
    private RawImage _currentVideo;
    //private MediaPlayerCtrl _currentVideoManager;
    private RawImage _nextVideo;
    //private MediaPlayerCtrl _nextVideoManager;
    private bool _isMoving = false;
    private bool _isActivatedButtons = false;


    void Awake ()
    {
        for (int i = 0; i < videoPlayerList.Count; i++)
        {
            videoPlayerList[i].display.rectTransform.localPosition = new Vector2(2048 * i, 70);
        }
    }

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    public void ActivateButtons()
    {
        _isActivatedButtons = true;
        for (int i = 0; i < videoPlayerList.Count; i++)
        {
            videoPlayerList[i].ActivateButtons();
        }
    }

    public void DeactivateButtons()
    {
        _isActivatedButtons = false;
        for (int i = 0; i < videoPlayerList.Count; i++)
        {
            videoPlayerList[i].DeactivateButton();
        }
    }

    public void SetCurrentVideoId(int id)
    {
        _currentVideoId = id;
        rectTransform.localPosition = new Vector3(-2048 * _currentVideoId, 0.0f, 0.0f);
    }

    public void Play()
    {
        videoPlayerList[_currentVideoId].Play();
    }

    public void PrevVideo()
    {
        if(!_isMoving)
        {
            _isMoving = true;
            if (_currentVideoId == 0)
            {
                _currentVideoId = 0;
            }
            else
            {
                _currentVideoId -= 1;
                if (VideoChanged != null) VideoChanged(_currentVideoId);
            }
            rectTransform.DOLocalMoveX(-2048 * _currentVideoId, 0.5f).SetEase(Ease.InOutQuart).OnComplete(OnMoved);
        }

        StopVideo();
    }

    public void NextVideo()
    {
        if(!_isMoving)
        {
            _isMoving = true;
            if (_currentVideoId == videoPlayerList.Count - 1)
            {
                _currentVideoId = videoPlayerList.Count - 10;
            }
            else
            {
                _currentVideoId += 1;
                if (VideoChanged != null) VideoChanged(_currentVideoId);
            }
            rectTransform.DOLocalMoveX(-2048 * _currentVideoId, 0.5f).SetEase(Ease.InOutQuart).OnComplete(OnMoved);
        }

        StopVideo();
    }

    private void OnMoved()
    {
        _isMoving = false;
        Play();
        if (VideoMoved != null) VideoMoved(0);
    }

    public void StopVideo()
    {
        for (int i = 0; i < videoPlayerList.Count; i++ )
        {
            videoPlayerList[i].Stop();
        }
    }

	public void OnVideoEvent(MediaPlayer mp, MediaPlayerEvent.EventType et)
	{
		switch (et) {
		case MediaPlayerEvent.EventType.ReadyToPlay:
			break;
		case MediaPlayerEvent.EventType.Started:
			break;
		case MediaPlayerEvent.EventType.FirstFrameReady:
			break;
		case MediaPlayerEvent.EventType.FinishedPlaying:
			break;
		}	
	}
}
