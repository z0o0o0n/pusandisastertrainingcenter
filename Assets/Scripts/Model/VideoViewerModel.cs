﻿using UnityEngine;
using System.Collections;

public class VideoViewerModel : MonoBehaviour 
{
    public delegate void VideoViewerEvent();
    public event VideoViewerEvent Prev;
    public event VideoViewerEvent Next;
    public delegate void VideoEvent(int id);
    public event VideoEvent BiginVideo;

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    public void SetVideo(int id)
    {
        if (BiginVideo != null) BiginVideo(id);
    }

    public void NextVideo()
    {
        if (Prev != null) Prev();
    }

    public void PrevVideo()
    {
        if (Next != null) Next();
    }
}
