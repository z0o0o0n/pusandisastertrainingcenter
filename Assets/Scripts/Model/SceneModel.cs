﻿using UnityEngine;
using System.Collections;

public class SceneModel : MonoBehaviour 
{
    public delegate void HomeEvent();
    public event HomeEvent MainPageOpen;
    public delegate void PageEvent(int id);
    public event PageEvent DetailPageOpen;
    public delegate void PageChangeEvent(int id);
    public event PageChangeEvent BeginChangeDetailPage;
    public delegate void SceneEvent();
    public event SceneEvent DetailPageInitialized;
    public event SceneEvent MainPageInitialized;

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    public void OpenMainPage()
    {
        if (MainPageOpen != null) MainPageOpen();
    }

    public void OpenDetailPage(int id)
    {
        if (DetailPageOpen != null) DetailPageOpen(id);
    }

    public void ChangeDetailPage(int id)
    {
        if (BeginChangeDetailPage != null) BeginChangeDetailPage(id);
    }

    public void InitMainPage()
    {
        if (MainPageInitialized != null) MainPageInitialized();
    }

    public void InitDetailPage()
    {
        if (DetailPageInitialized != null) DetailPageInitialized();
    }
}
