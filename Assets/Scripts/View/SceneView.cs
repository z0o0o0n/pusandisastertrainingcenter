﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class SceneView : MonoBehaviour 
{
    public SceneModel model;
    public BgManager bgManager;
    public BarManager barManager;
    public RectTransform mainPageRT;
    public RectTransform detailPageRT;

    void Awake ()
    {
        model.MainPageOpen += OnMainPageOpen;
        model.DetailPageOpen += OnDetailPageOpen;

        Vector3 videoViewerPos = detailPageRT.localPosition;
        videoViewerPos.y = -1500;
        detailPageRT.localPosition = videoViewerPos;
    }

	void Start () 
	{

	}
	
	void Update () 
	{
	
	}

    private void OnMainPageOpen()
    {
        mainPageRT.DOLocalMoveY(0, 0.5f).SetEase(Ease.InOutQuart);
        detailPageRT.DOLocalMoveY(-1500, 0.5f).SetEase(Ease.InOutQuart).OnComplete(OnMainPageOpened);
    }

    private void OnMainPageOpened()
    {
        model.InitMainPage();
    }

    private void OnDetailPageOpen(int id)
    {
        mainPageRT.DOLocalMoveY(1500, 0.5f).SetEase(Ease.InOutQuart);
        detailPageRT.DOLocalMoveY(0, 0.5f).SetEase(Ease.InOutQuart).OnComplete(OnDetailPageOpened);
    }

    private void OnDetailPageOpened()
    {
        model.InitDetailPage();
    }

    void OnDestory()
    {
        model.MainPageOpen -= OnMainPageOpen;
        model.DetailPageOpen -= OnDetailPageOpen;
    }
}
