﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine.UI;

public class DetailPage : MonoBehaviour
{
    public SceneModel model;
    public VideoPlayerManager videoPlayerManager;
    public Image prevButtonImage;
    public Image nextButtonImage;
	public SoundManager soundManager;

    private bool _isActivatedButtons = false;

    void Awake ()
    {
        model.DetailPageOpen += OnDetailPageOpen;
        model.DetailPageInitialized += OnDetailPageInitialized;
        videoPlayerManager.VideoChanged += OnVideoChanged;
        videoPlayerManager.VideoMoved += OnVideoMoved;

		ActivationCheckManager.Deactivated += OnUserDeactivated;
    }

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

	private void OnUserDeactivated()
	{
		OnClickHomeButton ();
	}

    private void OnDetailPageOpen(int id)
    {
        OnVideoChanged(id);
        videoPlayerManager.SetCurrentVideoId(id);
    }

    private void OnDetailPageInitialized()
    {
        videoPlayerManager.Play();
        ActivateButtons();
    }

    private void OnVideoChanged(int videoId)
    {
		/**
        if (videoId == 0)
        {
            prevButtonImage.gameObject.SetActive(false);
            nextButtonImage.gameObject.SetActive(true);
        }
        else if (videoId == (videoPlayerManager.videoPlayerList.Count - 1))
        {
            prevButtonImage.gameObject.SetActive(true);
            nextButtonImage.gameObject.SetActive(false);
        }
        else
        {
            prevButtonImage.gameObject.SetActive(true);
            nextButtonImage.gameObject.SetActive(true);
        }
        */
    }

    private void OnVideoMoved(int videoId)
    {
        ActivateButtons();
    }

    public void ActivateButtons()
    {
        _isActivatedButtons = true;
        
        videoPlayerManager.ActivateButtons();
    }

    public void DeactivateButtons()
    {
        _isActivatedButtons = false;
        videoPlayerManager.DeactivateButtons();
    }

    public void OnClickHomeButton()
    {
        if (_isActivatedButtons)
        {
            model.OpenMainPage();
            videoPlayerManager.StopVideo();

            DeactivateButtons();

            ActivationCheckManager.Stop();

			soundManager.PlayButtonSound();
        }
    }

    public void OnClickPrevButton()
    {
        if (_isActivatedButtons)
        {
            videoPlayerManager.PrevVideo();
            DeactivateButtons();
        }
    }

    public void OnClickNextButton()
    {
        if (_isActivatedButtons)
        {
            videoPlayerManager.NextVideo();
            DeactivateButtons();
        }
    }

    void OnDestroy()
    {
        model.DetailPageOpen -= OnDetailPageOpen;
		ActivationCheckManager.Deactivated -= OnUserDeactivated;
    }
}
