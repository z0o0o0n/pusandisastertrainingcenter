﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class MainPage : MonoBehaviour 
{
    public SceneModel model;
	public Menu menu;
	public ModelImageDisplayer modelImageDisplayer;
	public ModelReturnManager modelReturnManager;
	public SoundManager soundManager;

    void Awake ()
    {
        model.MainPageOpen += OnMainPageOpen;
        model.MainPageInitialized += OnMainPageInitialized;
        model.DetailPageInitialized += OnDetailPageInitialized;

		menu.Selected += OnMenuSelected;

		modelImageDisplayer.PointerClick += OnPointerClick;
		modelImageDisplayer.Changed += OnModelImageChanged;

		modelReturnManager.Timeout += OnReturnTimeout;
    }

	void Start () 
	{
		menu.Select(0);
		OnMainPageOpen ();
	}
	
	void Update () 
	{
	
	}

    private void OnMainPageOpen()
    {
		if(menu.currentId != 0) modelReturnManager.StartCheck();
		else modelReturnManager.StopCheck();

		modelImageDisplayer.ShowLabelPointers();
    }

    private void OnMainPageInitialized()
    {
		menu.ActivateButtons();
    }

    private void OnDetailPageInitialized()
    {
		modelImageDisplayer.ResetLabelPointers();
    }

	private void OnMenuSelected(int id)
	{
		menu.DeactivateButtons();

		modelImageDisplayer.DeactivateButtons();
		modelImageDisplayer.Change(id);

		if(menu.currentId != 0) modelReturnManager.StartCheck();
		else modelReturnManager.StopCheck();

		soundManager.PlayButtonSound ();
	}

    private void OnPointerClick(int id)
    {
		menu.DeactivateButtons();
		modelImageDisplayer.DeactivateButtons();
        model.OpenDetailPage(id);

		soundManager.PlayButtonSound ();

		if(menu.currentId != 0) modelReturnManager.StopCheck();
    }

	private void OnModelImageChanged()
	{
		menu.ActivateButtons();
		modelImageDisplayer.ActivateButtons();
	}

	private void OnReturnTimeout()
	{
		menu.Select (0);
	}

    void OnDestory()
    {
		model.MainPageOpen -= OnMainPageOpen;
        model.MainPageInitialized -= OnMainPageInitialized;
        model.DetailPageInitialized -= OnDetailPageInitialized;

		menu.Selected -= OnMenuSelected;

		modelImageDisplayer.PointerClick -= OnPointerClick;

		modelReturnManager.Timeout -= OnReturnTimeout;
    }
}
